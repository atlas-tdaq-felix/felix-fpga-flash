################################################
## Modified by Kai Chen for FELIX FLX711 V1.5 ##
################################################

#!/bin/bash
#
# Heiko Engel, <hengel@cern.ch>
#
# Changelog:
# 2013-07-26 - HE - initial checkin
# 2013-07-31 - HE - added flash selection
# 2015-02-12 - HE - renamed, added chain selection support
#
# Usage:
#       ./crorc_smbus_reconfigure_fpga [i2c-chain] [slvaddr] ([flashsel])
# [i2c-chain] : (required) The i2c chain to be used
# [slvaddr]   : (required) The slave address of the microcontroller.
# [flashsel]  : (optional) Selects the flash from where the device will
#               be reconfigured. 
#               Possible values are 0 (->FLASH0) and 1 (FLASH1).
# Example:
# crorc_smbus_reconfigure_fpga 0 0x44 0

# I2C Chain:
# This corresponds to /dev/i2c-[CHAIN], 
# CHAIN=0 for /dev/i2c-0, CHAIN=1 for /dev/i2c-1, etc.
if [ -z $1 ]; then
  echo "Please provide I2C chain as 1st argument"
  exit
fi
CHAIN=$1

# I2C slave address (i.e. the uC)
if [ -z $2 ]; then
  echo "Please provide I2C slave address as 2nd argument"
  exit
fi
SLV_ADDR=$2

# The flash 'partition number' (flash base address) to set
if [ -z $3 ]; then
  echo "Please provide FLASH partition number as 3rd argument"
  exit
fi
FLASH_NR=$3

# Set the I2C switch on the FLX-711 v1.5: enable channel 0 towards the uC
/usr/sbin/i2cset -y $CHAIN 0x70 0x01

# uC register selection identifiers
PINC_REG=0x0b
PORTC_REG=0x11
DDRC_REG=0x10
#PIND_REG=0x0c
#PORTD_REG=0x13
#DDRD_REG=0x12

# Select uC port register-set to use from the definitions above
PIN_REG=$PINC_REG
PORT_REG=$PORTC_REG
DDR_REG=$DDRC_REG

# Drive FLASH_A25/A26 according to selected flash 'partition number'
DDR_VAL=0x18
if [ $FLASH_NR -eq 0 ]; then
  # FLASH_A25 low, FLASH_A26 low
  PORT_VAL=0x00
elif [ $FLASH_NR -eq 1 ]; then
  # FLASH_A25 high, FLASH_A26 low
  PORT_VAL=0x08
elif [ $FLASH_NR -eq 2 ]; then
  # FLASH_A25 low, FLASH_A26 high
  PORT_VAL=0x10
elif [ $FLASH_NR -eq 3 ]; then
  # FLASH_A25 high, FLASH_A26 high
  PORT_VAL=0x18
else
  # FLASH_A25 low, FLASH_A26 low
  PORT_VAL=0x00
fi

# Set I/Os
/usr/sbin/i2cset -y $CHAIN $SLV_ADDR $PORT_REG $PORT_VAL
# Set selected I/Os to output
/usr/sbin/i2cset -y $CHAIN $SLV_ADDR $DDR_REG $DDR_VAL

echo "==Status=="
# Get PINC register
echo "PINC ="`/usr/sbin/i2cget -y $CHAIN $SLV_ADDR $PIN_REG`
# Get PORTC register
echo "PORTC="`/usr/sbin/i2cget -y $CHAIN $SLV_ADDR $PORT_REG`
# Get DDRC register
echo "DDRC ="`/usr/sbin/i2cget -y $CHAIN $SLV_ADDR $DDR_REG`
echo ""
