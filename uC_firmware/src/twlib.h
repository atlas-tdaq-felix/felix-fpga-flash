/**
 * @file twlib.h
 * @author Heiko Engel <hengel@cern.ch>
 * @version 0.1
 * @date 2012-10-23
 *
 * @section LICENSE
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * */
#ifndef TWLIB_H
#define TWLIB_H

#define TWREAD 0
#define TWWRITE 1

void twSlaveInit(uint8_t dev_addr, uint8_t gc_enable);
uint8_t getTwInt(void);
uint8_t getTwBusy(void);
void handleTwi(void);
void sendTwByte( uint8_t data );

uint8_t getTwCmd( void );
uint8_t getTwAddr( void );
uint8_t getTwData( void );
void rstTwCmd( void );

#endif
