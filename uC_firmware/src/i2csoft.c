/**********************************************************

Software I2C Library for AVR Devices.

Copyright 2008-2012
eXtreme Electronics, India
www.eXtremeElectronics.co.in

Changelog:
2013-01-08 hengel: bugfix in SoftI2CInit(), minor rewrites
**********************************************************/

#include <avr/io.h>
#include <util/delay.h>

#include "i2csoft.h"

#define Q_DEL _delay_loop_2(3)
#define H_DEL _delay_loop_2(6)

void SoftI2CInit()
{
	SDAPORT&=~(1<<SDA);
	SCLPORT&=~(1<<SCL);

	SOFT_I2C_SDA_HIGH;
	SOFT_I2C_SCL_HIGH;

}

void SoftI2CStart()
{
	SOFT_I2C_SCL_HIGH;
	H_DEL;

	SOFT_I2C_SDA_LOW;
	H_DEL;
}

void SoftI2CStop()
{
	 SOFT_I2C_SDA_LOW;
	 H_DEL;
	 SOFT_I2C_SCL_HIGH;
	 Q_DEL;
	 SOFT_I2C_SDA_HIGH;
	 H_DEL;
}

uint8_t SoftI2CWriteByte(uint8_t data)
{

	 uint8_t i;
	 uint8_t ack;

	 for(i=0;i<8;i++)
	 {
		SOFT_I2C_SCL_LOW;
		Q_DEL;

		if(data & 0x80)
			SOFT_I2C_SDA_HIGH;
		else
			SOFT_I2C_SDA_LOW;

		H_DEL;

		SOFT_I2C_SCL_HIGH;
		H_DEL;

		while((SCLPIN & (1<<SCL))==0);

		data=data<<1;
	}

	//The 9th clock (ACK Phase)
	SOFT_I2C_SCL_LOW;
	Q_DEL;

	SOFT_I2C_SDA_HIGH;
	H_DEL;

	SOFT_I2C_SCL_HIGH;
	H_DEL;

	if (SDAPIN & (1<<SDA))
		ack = 0;
	else
		ack = 1;

	SOFT_I2C_SCL_LOW;
	H_DEL;

	return ack;

}


uint8_t SoftI2CReadByte(uint8_t ack)
{
	uint8_t data=0x00;
	uint8_t i;
	SOFT_I2C_SDA_HIGH;

	for(i=0;i<8;i++)
	{

		SOFT_I2C_SCL_LOW;
		H_DEL;
		SOFT_I2C_SCL_HIGH;
		H_DEL;

		while((SCLPIN & (1<<SCL))==0);

		data = (data << 1) + ((SDAPIN>>SDA) & 0x01);
	}

	SOFT_I2C_SCL_LOW;
	Q_DEL;

	if(ack)
		SOFT_I2C_SDA_LOW;
	else
		SOFT_I2C_SDA_HIGH;
	H_DEL;

	SOFT_I2C_SCL_HIGH;
	H_DEL;

	SOFT_I2C_SCL_LOW;
	H_DEL;

	SOFT_I2C_SDA_HIGH;

	return data;
}


uint8_t
SoftI2CReadMem
(
    uint8_t slvaddr,
    uint8_t memaddr
)
{
    SoftI2CInit();
    SoftI2CStart();

    /** send slave address + write */
    SoftI2CWriteByte(slvaddr<<1);
    /** send mem address */
    SoftI2CWriteByte(memaddr);
    /** send repeated start */
    SoftI2CStart(); 
    /** send slave address + read */
    SoftI2CWriteByte((slvaddr<<1) + 1);
    /** read from slave, set nACK */
    uint8_t val = SoftI2CReadByte(0);
   
    SoftI2CStop();

    return val;
}


void
SoftI2CWriteMem
(
    uint8_t slvaddr,
    uint8_t memaddr,
    uint8_t data
)
{
    SoftI2CInit();
    SoftI2CStart();

    /** send slave address + write */
    SoftI2CWriteByte(slvaddr<<1);
    /** send mem address */
    SoftI2CWriteByte(memaddr);
    /** send data */
    SoftI2CWriteByte(data);
    
    SoftI2CStop();
}


