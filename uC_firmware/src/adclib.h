/**
 * @file adclib.h
 * @author Heiko Engel <hengel@cern.ch>
 * @version 0.1
 * @date 2012-11-06
 *
 * @section LICENSE
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * */

#ifndef ADCLIB_H
#define ADCLIB_H

void adcInit( uint8_t channel );
uint8_t adcConversionDone( void );
uint8_t adcGetResult( void );

#endif
