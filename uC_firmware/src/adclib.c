/**
 * @file adclib.c
 * @author Heiko Engel <hengel@cern.ch>
 * @version 0.1
 * @date 2012-11-06
 *
 * @section LICENSE
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * */

#include<avr/io.h>
#include <stdlib.h>
#include <stdio.h>

#include "adclib.h"


void adcInit( uint8_t channel ) {
	ADMUX = (0<<REFS0) | (0<<REFS1) | //AREF, internal VREF turned off
		(1<<ADLAR) | //left adjust result
		(channel & 0x0f); //MUX[4:0]=channel
	ADCSRA = (1<<ADEN) | (1<<ADSC); //enable, start, prescaler="000"
}

uint8_t adcConversionDone( void ) {
	return ADCSRA & (1<<ADIF);
}

uint8_t adcGetResult( void ) {
	uint8_t result = ADCH;
	ADCSRA &= ~(1<<ADIF); //clear interrupt flag
	return result;
}
