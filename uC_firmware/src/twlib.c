/**
 * @file twlib.c
 * @author Heiko Engel <hengel@cern.ch>
 * @version 0.1
 * @date 2012-10-23
 *
 * @section LICENSE
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * */
#include <avr/io.h>
#include <stdlib.h>
#include <stdio.h>

#include "twlib.h"

uint8_t twaddr = 0;
uint8_t twcmd = 0;
uint8_t twdata = 0;

uint8_t twi_addr_set = 0;

void twSlaveInit(uint8_t dev_addr, uint8_t gc_enable)
{
	// set device's own slave address to TWAR[7:1]
	// TWAR[0]: if set, device will respone to general
	// call address (0x00)
	TWAR = (dev_addr<<1) | (gc_enable & 0x01);
	TWCR = (1<<TWEA) | //enable acknowledgement
		(1<<TWEN); // TWI enable
}

uint8_t getTwInt(void)
{
	return (TWCR>>TWINT) & 0x01;
}

void sendTwByte( uint8_t data )
{
	TWDR = data;
	TWCR = (1<<TWEN) | (1<<TWINT) | (1<<TWEA);
}

uint8_t getTwCmd( void ) {
	return twcmd;
}

uint8_t getTwAddr( void ) {
	return twaddr;
}

uint8_t getTwData( void ) {
	return twdata;
}

void rstTwCmd( void ) {
	twcmd = 0;
}

void handleTwi(void)
{
		switch (TWSR & 0xfc) //mask prescaler bits
		{
			case (0x60): //own SLA+W received, ACK returned
				twi_addr_set=0;
				TWCR = (1<<TWEN) | (1<<TWINT) |	(1<<TWEA);
				break;

			case (0x68):
				//arbitration lost
				TWCR = (1<<TWEN) | (1<<TWINT) | (1<<TWEA);
				break;

			case (0x80): 
				// previously addressed with own SLA+W, data received, ACK returned
				if(!twi_addr_set) {
					twaddr = TWDR;
					twi_addr_set = 1;
				} else {
					twdata = TWDR; // read TWDR
					twcmd = (1<<TWWRITE);
				}
				TWCR = (1<<TWEN) | (1<<TWINT) | (1<<TWEA);
				break;

			case (0x88):
				//previously addressed with own SLA+W, data received, NACK returned
				if(!twi_addr_set) {
					twaddr = TWDR;
				} else {
					twdata = TWDR; // read TWDR
					twcmd = (1<<TWWRITE);
				}
				TWCR = (1<<TWEN) | (1<<TWINT) | (1<<TWEA);
				break;

			case (0xa0):
				// STOP or repeated START while addressed as slave
				TWCR = (1<<TWEN) | (1<<TWINT) | (1<<TWEA);
				break;

			case (0xa8):
				//own SLA+R, ACK returned
				twcmd = (1<<TWREAD);
				// load TWDR
				break;

			case (0xb0):
				// Arbitration lost
				twcmd = (1<<TWREAD);
				// load TWDR
				break;

			case (0xb8):
				// data byte in TWDR has been transmitted, ACK received
				//twaddr++;
				twcmd = (1<<TWREAD);
				break;

			case (0xc0):
				//data byte in TWDR transmitted, NACK received
				TWCR = (1<<TWEN) | (1<<TWINT) | (1<<TWEA);
				break;

			case (0xc8):
				// last data byte in TWDR has been transmitted, ACK received
				TWCR = (1<<TWEN) | (1<<TWINT) | (1<<TWEA);
				break;

			case (0x00):
				// bus error, illegal STA/STO condition
				TWCR = (1<<TWEN) | (1<<TWINT) | (1<<TWEA) | (1<<TWSTO);
				break;

		} //switch
}


