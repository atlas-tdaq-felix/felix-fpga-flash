The code here is for the FPGA firmware version control. It includes:
- the C code running in the micro-controller, and the files (.bin, .elf and .hex) to program the micro-controller.
- the .sh file to program the FPGA from 1 of the 4 bit files in the Flash.
- the tool to update the bit file in the Flash via the PCIe interface.
